// use memmap;
// use std::fs::File;
use yaml_rust::Yaml;

/// Possible status of a stepper driver
pub enum StepperStatus {
    /// Disabled
    StandBy,
    /// Enabled, waiting
    Ready,
    /// Moving
    Busy,
    /// Movement paused
    Paused,
    /// Something gone wrong
    Error
}

/// A stepper driver instance structure
pub struct  Stepper {
    /// The base memory address used to access the correct register in the /dev/mem device
    base_address: i64,   
    /// Convert number of steps to the chosen unit: it depends from the mecanical transmission
    /// and the micro stepping configuration
    step_unit_ratio: f64,
    /// Base clock frequency,
    base_clock: f64,    

    // Memory Device access
    // mmap: memmap::Mmap
    // stauts_reg
    // config_reg
    // 
    // 
}

impl Stepper {
    pub fn new(
        base_address: i64, 
        step_unit_ratio: f64,
        base_clock: f64    
    ) -> Stepper {
        // let mut file = File::open("/dev/mem")?;
        Stepper {
            base_address: base_address,
            step_unit_ratio: step_unit_ratio,
            base_clock: base_clock,
        }
    }

    pub fn from_json(object: Yaml) -> Option<Stepper> {
        let base_address : i64;
        match object["base_address"]{ 
            Yaml::Integer(x) => base_address = x, 
            _ => return None
        } 
        let step_to_unit : f64;
        match object["step_to_unit"] {
            Yaml::Real(_) => step_to_unit = object["step_to_unit"].as_f64()?,
            _ => return None
        }  
        let base_clock : f64;
        match object["base_clock"] {
            Yaml::Real(_) => base_clock = object["base_clock"].as_f64()?,
            _ => return None
        }

        Some(Stepper::new(
            base_address,
            step_to_unit,
            base_clock
        ))
    }

    pub fn base_address(& self) -> i64 {
        self.base_address
    }

    pub fn base_clock(& self) -> f64 {
        self.base_clock
    }

    pub fn step_to_unit(& self) -> f64 {
        self.step_unit_ratio
    }
}

// let mmap = unsafe {
//     MmapOptions::new()
//                 .offset(PERIPHERAL_BASE_ADDRESS + SYSTEM_TIMER_OFFSET + 4)
//                 .len(4096)
//                 .map_mut(&f)
//                 .unwrap()
// };

// let ptr = mmap.as_ptr() as *const u32;
// loop {
//     let data: u32 = unsafe { ptr.read_volatile() };
// println!("{}", data);
// }
