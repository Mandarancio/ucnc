use std::env;

extern crate cncservice;

fn main() -> Result<(), cncservice::error::MyError> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        Err(cncservice::error::MyError::new("missing arguments!"))
    } else {
        let path = &args[1];
        match cncservice::processor::gcode::exec(path) {
            Ok(_) => Ok(()),
            Err(_) => Err(cncservice::error::MyError::new("Gcode execution failed"))
        }
    }
}
