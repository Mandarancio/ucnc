use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

use ::phf::{phf_map, Map};

use gcode;

/*
struct GCodeCommand {
    mnemonic : Mnemonic,
    number : f32
}*/

fn g0_eval(arguments: &[gcode::Word]) {
    print!("G0");
    for argument in arguments {
        print!(" {}", argument);
    }
    println!("");
}

fn g30_eval(arguments: &[gcode::Word]) {
    print!("G30");
    for argument in arguments {
        print!(" {}", argument);
    }
    println!("");
}

type Callback = fn(&[gcode::Word]);

static COMMANDS: Map<&'static str, Callback> = phf_map! {
   // GCodeCommand(Mnemonic::General, 0) => "G0"
    "G0.0" => g0_eval,
    "G30.0" => g30_eval,
};

fn cmd2str(command: &gcode::GCode<>) -> String {
    return format!("{}{}.{}", 
                   command.mnemonic(), 
                   command.major_number(), 
                   command.minor_number());
}


pub fn exec(path: &String) -> std::io::Result<()> {
    println!("GCode path: {}", path);
    let file = File::open(path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    let got: Vec<_> = gcode::parse(&mut contents).collect();
    for command in got {
        let cstr = cmd2str(&command);
        if COMMANDS.contains_key(&cstr[..]) {
            COMMANDS[&cstr[..]](command.arguments());
        } else {
            println!("WARNING: command {} not found, skipping..", cstr);
        }
    }
    Ok(())
}
